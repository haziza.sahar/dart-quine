dart-quine
==========

This is a Quine in Dart.

A Quine is a computer program which prints its own source code.

See http://en.wikipedia.org/wiki/Quine_(computing) to get more information about Quines.
